from bs4    import BeautifulSoup
from urllib2 import urlopen, quote
from re     import sub
import sqlite3
import httplib2

import datetime
import PyRSS2Gen


base_url = 'http://omnisophie.com/'
folder = '/home/lb/public_html/dailydueck/'

# create DB-connection and crete table
conn = sqlite3.connect(folder+'dueck.db')
c = conn.cursor()
#                                                 0                  1             2          3          4           5         6           7              8 
c.execute('create table if not exists content(id int primary key, datetime text, tldr text, title text, text text, link text, pdf text, pdfsize int, bottomLinks text);')

# get all known ids, to avoid unnecessary requests
c.execute("select id from content;")
all_ids = list()
for id in c.fetchall(): 
  all_ids.append(str(id[0]))

# load source of archive-page
page = urlopen(base_url+'day_archiv.html')
source = page.read()
page.close()
# parse source
archive = BeautifulSoup(source)


all_articles = dict()

# iterate over the archive page to get the article-links
for li in archive.find_all('a'):
  if 'class' in li.attrs:
    if 'linkDD' in li['class']:

      # remove title from link-text
      # i.e.
      # Daily Dueck #152 - 'Leitlosigkeit in Deutschland - durch Tagesdenken'
      # --> 152
      id = li.attrs['href'][4:].partition('.')[0]
      id = sub('^0+','',id)

      # skip known id
      if id in all_ids: continue

      # link to article 
      # i.e.
      # http://www.omnisophie.com/day_152.html
      link = li.attrs['href']
      all_articles[id] = dict()
      all_articles[id]['link'] = base_url+link
      
      # follow the article-link and get the source
      tmp_p = urlopen(base_url+link)
      tmp_s = tmp_p.read()
      tmp_p.close()
      # parse source
      article = BeautifulSoup(tmp_s)

      # title of the article
      # i.e.
      # <h1> Leitlosigkeit in Deutschland - durch Tagesdenken (Daily Dueck 152, Oktober 2011)</h1>
      all_articles[id]['title'] = article.h1.string

      # the summary
      # i.e.
      # <h2>Wer einen Charakter hat, ... </h2>
      # this does not work for article #44 there is no h2
      if article.h2 == None:  all_articles[id]['tldr'] = 'no tldr'
      else:                   all_articles[id]['tldr'] = article.h2.string

      # get the text of the current arcticle
      text = unicode()
      
      for tag in article.find(id="contentText").children:
        try:
          if tag.name == 'p':
            for string in tag.strings:
              text += '<p>'+string+'</p>'
        except:
          pass  
      
      #for p in article.find(id="contentText").find_all('p'):
      #  text_tmp = unicode(p.string)
      #  if not text_tmp == 'None':
      #    text += text_tmp+'<br>'
      all_articles[id]['text'] = text

      noscript_tag = article.select("#bottomLinks")[0].noscript
      if not noscript_tag == None:
        all_articles[id]['bottomLinks'] = article.select("#bottomLinks")[0].noscript.text
      else:
        all_articles[id]['bottomLinks'] = ''
       
    # Link to pdf  
    if 'pdfDownload' in li['class']:
      id = li.attrs['href'][12:].partition('-')[0]

      # skip known id
      if id in all_ids: continue
      
      link = li.attrs['href']
      all_articles[id]['pdf'] = base_url+link.replace(' ','%20')

      # get pdfsize and date
      h = httplib2.Http()
      resp = h.request((base_url+link).replace(' ','%20'), 'HEAD')
      try:
        all_articles[id]['datetime'] = resp[0]['last-modified']
        all_articles[id]['pdfsize']= resp[0]['content-length']
      except KeyError, e:
        print base_url+link
        print resp
        all_articles[id]['pdfsize'] = 10

      # if article already in DB remove
      if(id in all_ids):
        c.execute("DELETE FROM content where id = ?",(id,)) 
      # insert all                           0 1 2 3 4 5 6 7 8     0                       1                          2                        3                         4                        5                       6                           7                            8     
      c.execute("INSERT INTO content VALUES (?,?,?,?,?,?,?,?,?)",(id,all_articles[id]['datetime'],all_articles[id]['tldr'],all_articles[id]['title'],all_articles[id]['text'],all_articles[id]['link'],all_articles[id]['pdf'],all_articles[id]['pdfsize'],all_articles[id]['bottomLinks'],))
      conn.commit()
      print "added "+id

#                   0      1     2     3    4      5        6           7
c.execute("SELECT title, tldr, text, link, pdf, pdfsize, datetime, bottomLinks FROM content ORDER BY id DESC")
fullfeed_items = list()
shortfeed_items = list()
for entry in c.fetchall():
  fullfeed_items.append(PyRSS2Gen.RSSItem(
    title       = entry[0],
    link        = entry[3],
    description = unicode(entry[1])+"<br><br>"+entry[2]+'<br><br>'+entry[7],
    guid        = PyRSS2Gen.Guid(entry[3]),
    enclosure   = PyRSS2Gen.Enclosure(entry[4], entry[5], "application/pdf"),
    pubDate     = entry[6]
  ))

  shortfeed_items.append(PyRSS2Gen.RSSItem(
    title       = entry[0],
    link        = entry[3],
    description = unicode(entry[1])+'<br><br><a href="'+entry[3]+'">weiter lesen</a>', # '<p>'+entry[1]+'</p>'+
    guid        = PyRSS2Gen.Guid(entry[3]),
    enclosure   = PyRSS2Gen.Enclosure(entry[4], entry[5], "application/pdf"),
    pubDate     = entry[6]
  ))

rss = PyRSS2Gen.RSS2(title = "SINNRAUM - Daily Dueck Archiv",link = "http://lb.bombenlabor.de/daily_dueck",description = "",lastBuildDate = datetime.datetime.now(),items = fullfeed_items)
rss.write_xml(open(folder+"feed.xml", "w"))

rss = PyRSS2Gen.RSS2(title = "SINNRAUM - Daily Dueck Archiv",link = "http://lb.bombenlabor.de/daily_dueck",description = "",lastBuildDate = datetime.datetime.now(),items = shortfeed_items)
rss.write_xml(open(folder+"short_feed.xml", "w"))

conn.close()
